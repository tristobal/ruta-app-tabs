angular.module('ruta.services', ['angular-storage'])

.service('SharedProperties', function (store) {
    var idUser = 0;
    var tokenExpiration = 0;
    var local = {};

    return {
        getLocal: function () {
            return local;
        },
        setLocal: function(value) {
            local = value;
        },
        getIdUser: function() {
            var token = store.get('jwt');
            if (token) {
                var txtPayloads = token.split('.')[1];
                var jswPayloads = decodeURIComponent(escape(window.atob( txtPayloads )));
                var payLoads = JSON.parse(jswPayloads);
                idUser = payLoads.sub;
            }
            return idUser;
        },
        //@TODO DEPRECATED
        setIdUser: function(value) {
            idUser = value;
        },
        getTokenExpiration: function() {
            var token = store.get('jwt');
            if (token) {
                var txtPayloads = token.split('.')[1];
                var jswPayloads = decodeURIComponent(escape(window.atob( txtPayloads )));
                var payLoads = JSON.parse(jswPayloads);
                tokenExpiration = payLoads.exp;
            }
            return tokenExpiration;
        },
        //@TODO DEPRECATED
        setTokenExpiration: function(value) {
            tokenExpiration = parseInt(value);
        }
    };
});
